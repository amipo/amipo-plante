# amipo-plante

Quand AMIPO plante, contactez nous ici.
Donnez-nous vos retours utilisateurs.

## Aidez nous, contribuez
Si vous souhaitez nous aider et contribuer à améliorer la qualités des services de l'AMIPO, voici le genre de tâches que vous pouvez faire. 
Elles sont classés du plus facile pour débuter, au plus compliqué pour les experts.

1. Si vous rencontrez un problème ou avez une proposition d'amélioration, vous pouvez :
   - [déclarer un nouveau tiquet via l'interface web](https://framagit.org/amipo/amipo-plante/issues/new)
   - [déclarer un tiquet sans compte par email](mailto:gitlab-incoming+amipo-amipo-plante-55401-JtTxU6rv7nCVf_mEnbzv-issue@framagit.org)
1. Vous pouvez donner votre avis sur [les tiquets "Avis demandé"](https://framagit.org/groups/amipo/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Avis%20demand%C3%A9)  
1. Vous pouvez nous aider à vérifier nos correctifs et solutions avec :
   - [lister les tiquets "A tester"](https://framagit.org/groups/amipo/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=To%20Test) 
   - [lister les tiquets "A passer en revue"](https://framagit.org/groups/amipo/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=To%20Review)
1. Si vous êtes familié avec les technologies du web, vous pouvez :
   - [commenter les tiquets "à étudier"](https://framagit.org/groups/amipo/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=A%20%C3%A9tudier)
   - [apporter des solutions aux tiquets "à faire"](https://framagit.org/groups/amipo/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=To%20Do)

## Apprenez
Vous pouvez apprendre depuis votre fauteueil en parcourant : 
1. [les documentations de l'AMIPO](https://amipo.fr/docs/)
1. [les tiquets fermés auquels nous avons apporté une sollution](https://framagit.org/groups/amipo/-/issues?scope=all&utf8=%E2%9C%93&state=closed) et leur commentaires

## Co-Administrez
1. [Parcourez le "board de tiquets global"](https://framagit.org/groups/amipo/-/boards) pour suivre l'avancement des travaux en cours et y apporter votre aide
1. Parcourez les "boards de tiquets" de chaque projet